/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import interfaces.Interface_Cliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.conexion.Conexion;
import modelo.entidades.Administrador;
import modelo.entidades.Cliente;
import modelo.entidades.Reservaciones;

/**
 *
 * @author BLADIMIR
 */
public class Dao_Cliente implements Interface_Cliente {

    public Conexion conexion;
    private Cliente cliente;
    //private Reservaciones reservaciones;
    private List<Cliente> lista_Cliente;

    public Dao_Cliente() {
    }

    public Dao_Cliente(Cliente cliente) {
        conexion = new Conexion();
        this.cliente = cliente;
    }
/*-----------------------------------------------------------------------*/
    @Override
    public Cliente obtener_Cliente() throws SQLException {
        try {

            Statement st = this.conexion.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT*FROM cliente\n"
                    + "WHERE cliente.dui_cliente = '" + cliente.getDui_cliente() + "'");
            cliente = new Cliente();
            while (rs.next()) {
                cliente.setDui_cliente(rs.getString("dui_cliente"));
                cliente.setNombre_cliente(rs.getString("nombre_cliente"));
                cliente.setApellido_cliente(rs.getString("apellido_cliente"));
                cliente.setContraseña_cliente(rs.getString("contrasena_cliente"));
                cliente.setTelefono_cliente(rs.getString("telefono_cliente"));
                cliente.setCorreo_cliente(rs.getString("correo_electronico"));
            }
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return cliente;
    }

    @Override
    public void insertar_Cliente() throws SQLException {
        try {
            Statement st = this.conexion.getConexion().createStatement();
            String sql = "INSERT INTO cliente(dui_cliente, nombre_cliente, apellido_cliente, contrasena_cliente, telefono_cliente, correo_electronico) \n"
                    + "VALUES (\n"
                    + "'" + cliente.getDui_cliente() + "',\n"
                    + "'" + cliente.getNombre_cliente() + "',\n"
                    + "'" + cliente.getApellido_cliente() + "',\n"
                    + "'" + cliente.getContraseña_cliente() + "',\n"
                    + "'" + cliente.getTelefono_cliente() + "',\n"
                    + "'" + cliente.getCorreo_cliente() + "')";
            st.executeUpdate(sql);
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void modificar_Cliente() throws SQLException {
        try {
            Statement st = this.conexion.getConexion().createStatement();
            st.executeUpdate("UPDATE cliente\n"
                    + "SET nombre_cliente='" + cliente.getNombre_cliente() + "',\n"
                    + "apellido_cliente='" + cliente.getApellido_cliente() + "',\n"
                    + "contrasena_cliente='" + cliente.getContraseña_cliente() + "',\n"
                    + "telefono_cliente='" + cliente.getTelefono_cliente() + "',\n"
                    + "correo_electronico='" + cliente.getCorreo_cliente() + "' \n"
                    + "WHERE (dui_cliente='" + cliente.getDui_cliente() + "')");
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void elimininar_Cliente() throws SQLException {
        try {
            Statement st = this.conexion.getConexion().createStatement();
            st.executeUpdate("DELETE FROM cliente WHERE (dui_cliente='" + cliente.getDui_cliente() + "')");
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public List<Cliente> mostrar_Cliente() throws SQLException {
        lista_Cliente = new ArrayList<>();
        ResultSet rs = null;
        try {
            conexion = new Conexion();
            Connection accesoDB = conexion.getConexion();
            String sql = "SELECT*FROM cliente";
            PreparedStatement ps = accesoDB.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                cliente = new Cliente();

                cliente.setDui_cliente(rs.getString("dui_cliente"));
                cliente.setNombre_cliente(rs.getString("nombre_cliente"));
                cliente.setApellido_cliente(rs.getString("apellido_cliente"));
                cliente.setContraseña_cliente(rs.getString("contrasena_cliente"));
                cliente.setTelefono_cliente(rs.getString("telefono_cliente"));
                cliente.setCorreo_cliente(rs.getString("correo_electronico"));

                lista_Cliente.add(cliente);
            }
            Conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return lista_Cliente;
    }

    /*PARA EL LOGGIN DEL CLIENTE*/
    public Cliente validar(String usuario, String contrasena_admin) {
        Cliente ad = new Cliente();
        ResultSet rs = null;
        String sql = "SELECT* FROM cliente WHERE dui_cliente = ? AND contrasena_cliente = ? ";
        try {
            conexion = new Conexion();
            Connection accesoDB = conexion.getConexion();
            PreparedStatement ps = accesoDB.prepareStatement(sql);
            ps.setString(1, usuario);
            ps.setString(2, contrasena_admin);
            rs = ps.executeQuery();
            while (rs.next()) {
                ad.setDui_cliente(rs.getString("dui_cliente"));
                ad.setNombre_cliente(rs.getString("nombre_cliente"));
                ad.setContraseña_cliente(rs.getString("contrasena_cliente"));
            }
        } catch (Exception e) {
        }
        return ad;
    }
}
