/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import interfaces.InterfaceClasificacion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.conexion.Conexion;
import modelo.entidades.Clasificacion;

/**
 *
 * @author CRUZ
 */
public class DAOClasificacion implements InterfaceClasificacion{
 public Conexion conexion;
    private Clasificacion clasi;
    private List<Clasificacion> listaClasif;
    @Override
    public List<Clasificacion> mostrar_Clasificacion() throws SQLException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        listaClasif = new ArrayList<>();
        ResultSet rs = null;
        try {
            conexion = new Conexion();
            Connection accesoDB = conexion.getConexion();
            String sql = "SELECT*FROM clasificacion";
            PreparedStatement ps = accesoDB.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                clasi = new Clasificacion();

                clasi.setIdClasificacion(Integer.parseInt(rs.getString("id_clasificacion")));
                clasi.setNombreClas(rs.getString("nombre_clasificacion"));
               

                listaClasif.add(clasi);
            }
            Conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return listaClasif;
    }
    
}
