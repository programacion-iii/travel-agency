/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo.dao;

import interfaces.InterfaceDetalleExcursion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.conexion.Conexion;
import modelo.entidades.Cliente;
import modelo.entidades.DetalleExcursion;
import modelo.entidades.Lugares;
import modelo.entidades.Motorista;
import modelo.entidades.Reservaciones;
import modelo.entidades.Transporte;

/**
 *
 * @author EQUIPO
 */
public class DAODetalleExcursion implements InterfaceDetalleExcursion{
    
    public Conexion conexion;
    private DetalleExcursion detalle;
    private Reservaciones reserva;
    private Motorista motorista;
    private Transporte transporte;
    
    private Lugares lugar;
    private Cliente cliente;
    
    private List<Transporte> listaTransporte;
    private List<Motorista> listaMotorista;
    private List<DetalleExcursion> listaDetalle;
    private List<Reservaciones> listaReservacion;

    public DAODetalleExcursion() {
    }

    public DAODetalleExcursion(DetalleExcursion detalle) {
        conexion=new Conexion();
        this.detalle = detalle;
    }

    @Override
    public DetalleExcursion obtenerDetalle() throws SQLException {
        try {
            Statement st = this.conexion.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM detalle_excursion WHERE id_detalle_excursion='" +detalle.getIddetalle() + "'");
            transporte = new Transporte();
            while (rs.next()) {

                reserva = new Reservaciones();
                reserva.setId_reservaciones(rs.getInt("id_reservaciones"));
                transporte= new Transporte();
                transporte.setPlaca(rs.getString("placa"));
                motorista=new Motorista();
                motorista.setDui_motorista(rs.getString("dui_motorista"));
                
                detalle.setIddetalle(rs.getInt("id_detalle_excursion"));
                detalle.setDuimotorista(motorista);
                detalle.setPlaca(transporte);
                detalle.setIdreservacion(reserva);
                detalle.setConfirmacion(rs.getBoolean("confirmar_salida"));

            }
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return detalle;
    }

    @Override
    public void insertarDetalle() throws SQLException {
        try {
            Statement st = this.conexion.getConexion().createStatement();
            String sql = "INSERT INTO detalle_excursion (id_detalle_excursion, id_reservaciones, placa, dui_motorista, confirmar_salida) VALUES ('" + detalle.getIddetalle() + "', '" + detalle.getIdreservacion().getId_reservaciones() + "','" + detalle.getPlaca().getPlaca() + "','"+ detalle.getDuimotorista().getDui_motorista() +"', '"+ detalle.isConfirmacion() +"')";
            st.executeUpdate(sql);
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void modificarDetalle() throws SQLException {
       try {
            Statement st = this.conexion.getConexion().createStatement();
            st.executeUpdate("UPDATE detalle_excursion SET id_reservaciones='" + detalle.getIdreservacion().getId_reservaciones() + "', placa='" + detalle.getPlaca().getPlaca() + "', dui_motorista='"+ detalle.getDuimotorista().getDui_motorista() +"', confirmar_salida='"+ detalle.isConfirmacion() +"' WHERE (id_detalle_excursion='" + detalle.getIddetalle() + "')");
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void eliminarDetalle() throws SQLException {
        try {
            Statement st = this.conexion.getConexion().createStatement();
            st.executeUpdate("DELETE FROM detalle_excursion WHERE (id_detalle_excursion='" + detalle.getIddetalle() + "')");
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public List<DetalleExcursion> mostrarDetalle() throws SQLException {
       listaDetalle = new ArrayList<>();
        ResultSet rs = null;
        try {
            conexion = new Conexion();
            Connection accesoDB = conexion.getConexion();
            String sql = "SELECT a.id_detalle_excursion, a.confirmar_salida, b.id_reservaciones, c.placa, d.dui_motorista, d.nombre_motorista, d.apellido_motorista, e.nombre_lugar, f.nombre_cliente, f.apellido_cliente FROM detalle_excursion AS a INNER JOIN reservaciones AS b	ON a.id_reservaciones = b.id_reservaciones INNER JOIN transporte AS c ON a.placa = c.placa INNER JOIN motorista AS d ON a.dui_motorista = d.dui_motorista INNER JOIN lugares AS e ON b.id_lugar = e.id_lugar INNER JOIN cliente AS f ON b.dui_cliente = f.dui_cliente";
            PreparedStatement ps = accesoDB.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                detalle = new DetalleExcursion();
                reserva = new Reservaciones();
                lugar=new Lugares();
                cliente=new Cliente();
                transporte=new Transporte();
                motorista=new Motorista();
                
                reserva.setId_reservaciones(rs.getInt("id_reservaciones"));
                lugar.setNombrelugar(rs.getString("nombre_lugar"));
                reserva.setLugares(lugar);
                cliente.setNombre_cliente(rs.getString("nombre_cliente"));
                cliente.setApellido_cliente(rs.getString("apellido_cliente"));
                reserva.setCliente(cliente);
                transporte.setPlaca(rs.getString("placa"));
                motorista.setNombre_motorista(rs.getString("nombre_motorista"));
                motorista.setApellido_motorista(rs.getString("apellido_motorista"));
                
                detalle.setDuimotorista(motorista);
                detalle.setPlaca(transporte);
                detalle.setConfirmacion(rs.getBoolean("confirmar_salida"));
                detalle.setIddetalle(rs.getInt("id_detalle_excursion"));
                detalle.setIdreservacion(reserva);
                listaDetalle.add(detalle);
            }
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return listaDetalle;
    }
    
}
