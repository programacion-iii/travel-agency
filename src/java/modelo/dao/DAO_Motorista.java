/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import interfaces.Interface_Motorista;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.conexion.Conexion;
import modelo.entidades.Motorista;

/**
 *
 * @author pc
 */
public class DAO_Motorista implements Interface_Motorista {

    public Conexion conexion;
    private Motorista motorista;
    private List<Motorista> lista_Motorista;

    public DAO_Motorista() {
    }

    public DAO_Motorista(Motorista motorista) {
        conexion = new Conexion();
        this.motorista = motorista;
    }

    @Override
    public Motorista obtener_Motorista() throws SQLException {
        try {

            Statement st = this.conexion.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT*FROM motorista\n"
                    + "WHERE motorista.dui_motorista = '" + motorista.getDui_motorista() + "'");
            motorista = new Motorista();
            while (rs.next()) {
                motorista.setDui_motorista(rs.getString("dui_motorista"));
                motorista.setNombre_motorista(rs.getString("nombre_motorista"));
                motorista.setApellido_motorista(rs.getString("apellido_motorista"));
                motorista.setTelefono(rs.getString("telefono"));
                motorista.setDireccion_motorista(rs.getString("direccion_motorista"));
                motorista.setDisponibilidad_motorista(rs.getBoolean("disponibilidad_motorista"));
            }
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return motorista;
    }

    @Override
    public void insertar_Motorista() throws SQLException {
        try {
            Statement st = this.conexion.getConexion().createStatement();
            String sql = "INSERT INTO motorista(dui_motorista, nombre_motorista, apellido_motorista, telefono, direccion_motorista, disponibilidad_motorista) \n"
                    + "VALUES (\n"
                    + "'" + motorista.getDui_motorista() + "',\n"
                    + "'" + motorista.getNombre_motorista() + "',\n"
                    + "'" + motorista.getApellido_motorista() + "',\n"
                    + "'" + motorista.getTelefono() + "',\n"
                    + "'" + motorista.getDireccion_motorista() + "',\n"
                    + "'" + motorista.getDisponibilidad_motorista() + "')";
            st.executeUpdate(sql);
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void modificar_Motorista() throws SQLException {
        try {
            Statement st = this.conexion.getConexion().createStatement();
            st.executeUpdate("UPDATE motorista\n"
                    + "SET nombre_motorista='" + motorista.getNombre_motorista() + "',\n"
                    + "apellido_motorista='" + motorista.getApellido_motorista() + "',\n"
                    + "telefono='" + motorista.getTelefono() + "',\n"
                    + "direccion_motorista='" + motorista.getDireccion_motorista() + "',\n"
                    + "disponibilidad_motorista='" + motorista.getDisponibilidad_motorista() + "' \n"
                    + "WHERE (dui_motorista='" + motorista.getDui_motorista() + "')");
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void elimininar_Motorista() throws SQLException {
        try {
            Statement st = this.conexion.getConexion().createStatement();
            st.executeUpdate("DELETE FROM motorista WHERE (dui_motorista='" + motorista.getDui_motorista() + "')");
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public List<Motorista> mostrar_Motorista() throws SQLException {
        lista_Motorista = new ArrayList<>();
        ResultSet rs = null;
        try {
            conexion = new Conexion();
            Connection accesoDB = conexion.getConexion();
            String sql = "SELECT*FROM motorista";
            PreparedStatement ps = accesoDB.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                motorista = new Motorista();

                motorista.setDui_motorista(rs.getString("dui_motorista"));
                motorista.setNombre_motorista(rs.getString("nombre_motorista"));
                motorista.setApellido_motorista(rs.getString("apellido_motorista"));
                motorista.setTelefono(rs.getString("telefono"));
                motorista.setDireccion_motorista(rs.getString("direccion_motorista"));
                motorista.setDisponibilidad_motorista(rs.getBoolean("disponibilidad_motorista"));

                lista_Motorista.add(motorista);
            }
            Conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return lista_Motorista;
    }
}
