/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.conexion.Conexion;
import modelo.entidades.Cliente;
import modelo.entidades.Reservaciones;
import interfaces.Interface_Reservaciones;
import modelo.entidades.Lugares;

/**
 *
 * @author BLADIMIR
 */
public class DAO_Reservaciones implements Interface_Reservaciones {

    public Conexion conexion;
    private Reservaciones reservacion;
    private Cliente cliente;
    private Lugares lugares;
    private List<Reservaciones> lista_reservacion;
    private List<Reservaciones> lista_reservacion2,listaReservacion3;

    public DAO_Reservaciones() {
    }

    public DAO_Reservaciones(Reservaciones reservacion) {
        conexion = new Conexion();
        this.reservacion = reservacion;
    }

    @Override
    public Reservaciones obtener_Reservacion() throws SQLException {
        try {

            Statement st = this.conexion.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT*FROM reservaciones WHERE\n"
                    + "reservaciones.id_reservaciones = '" + reservacion.getId_reservaciones() + "'");
            reservacion = new Reservaciones();
            while (rs.next()) {

                reservacion.setId_reservaciones(rs.getInt("id_reservaciones"));
                reservacion.setDui_cliente(rs.getString("dui_cliente"));
                reservacion.setId_lugar(rs.getInt("id_lugar"));
                reservacion.setMonto(rs.getFloat("monto"));
                reservacion.setHora(rs.getString("hora"));
                reservacion.setFecha(rs.getString("fecha"));
                reservacion.setDireccion_encuentro(rs.getString("direccion_encuentro"));
            }
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return reservacion;
    }

    @Override
    public void insertar_Reservacion() throws SQLException {
        try {
            Statement st = this.conexion.getConexion().createStatement();
            String sql = "INSERT INTO reservaciones (id_reservaciones, dui_cliente, id_lugar, monto, hora, fecha, direccion_encuentro) \n"
                    + "VALUES ('" + reservacion.getId_reservaciones() + "',\n"
                    + " '" + reservacion.getCliente().getDui_cliente() + "',\n"
                    + " '" + reservacion.getLugares().getIdLugares() + "',\n"
                    + " '" + reservacion.getMonto() + "',\n"
                    + " '" + reservacion.getHora() + "',\n"
                    + " '" + reservacion.getFecha() + "',\n"
                    + " '" + reservacion.getDireccion_encuentro() + "')";
            st.executeUpdate(sql);
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void modificar_Reservacion() throws SQLException {
        try {
            Statement st = this.conexion.getConexion().createStatement();
            st.executeUpdate("UPDATE reservaciones\n"
                    + "SET\n"
                    + "dui_cliente='" + reservacion.getCliente().getDui_cliente() + "',\n"
                    + "id_lugar='" + reservacion.getLugares().getIdLugares() + "',\n"
                    + "monto='" + reservacion.getMonto() + "',\n"
                    + "hora='" + reservacion.getHora() + "',\n"
                    + "fecha='" + reservacion.getFecha() + "',\n"
                    + "direccion_encuentro='" + reservacion.getDireccion_encuentro() + "' \n"
                    + "WHERE (id_reservaciones='" + reservacion.getId_reservaciones() + "')");
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void elimininar_Reservacion() throws SQLException {
        try {
            Statement st = this.conexion.getConexion().createStatement();
            st.executeUpdate("DELETE FROM reservaciones WHERE (id_reservaciones='" + reservacion.getId_reservaciones() + "')");
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public List<Reservaciones> mostrar_Reservacion() throws SQLException {
        lista_reservacion = new ArrayList<>();
        ResultSet rs = null;
        try {
            conexion = new Conexion();
            Connection accesoDB = conexion.getConexion();
            String sql = "SELECT\n"
                    + "reservaciones.id_reservaciones,\n"
                    + "cliente.nombre_cliente,\n"
                    + "lugares.nombre_lugar,\n"
                    + "reservaciones.hora,\n"
                    + "reservaciones.fecha,\n"
                    + "reservaciones.monto,\n"
                    + "reservaciones.direccion_encuentro\n"
                    + "FROM\n"
                    + "lugares\n"
                    + "INNER JOIN reservaciones ON reservaciones.id_lugar = lugares.id_lugar\n"
                    + "INNER JOIN cliente ON reservaciones.dui_cliente = cliente.dui_cliente";
            PreparedStatement ps = accesoDB.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                reservacion = new Reservaciones();
                cliente = new Cliente();
                lugares = new Lugares();

                cliente.setNombre_cliente(rs.getString("nombre_cliente"));
                lugares.setNombrelugar(rs.getString("nombre_lugar"));

                reservacion.setId_reservaciones(rs.getInt("id_reservaciones"));
                reservacion.setCliente(cliente);
                reservacion.setLugares(lugares);
                reservacion.setMonto(rs.getFloat("monto"));
                reservacion.setHora(rs.getString("hora"));
                reservacion.setFecha(rs.getString("fecha"));
                reservacion.setDireccion_encuentro(rs.getString("direccion_encuentro"));

                lista_reservacion.add(reservacion);
            }
            Conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return lista_reservacion;
    }

    public List<Reservaciones> mostrarReservacionSinConfirmar() throws SQLException {
        lista_reservacion2 = new ArrayList<>();
        ResultSet rs = null;
        try {
            conexion = new Conexion();
            Connection accesoDB = conexion.getConexion();
            String sql = "SELECT a.id_reservaciones, a.dui_cliente, a.id_lugar, a.monto, a.hora, a.fecha, a.direccion_encuentro, b.id_lugar, b.nombre_lugar, c.dui_cliente, c.nombre_cliente, c.apellido_cliente FROM reservaciones AS a INNER JOIN lugares AS b ON a.id_lugar = b.id_lugar INNER JOIN cliente AS c ON a.dui_cliente = c.dui_cliente WHERE NOT EXISTS (SELECT p.id_reservaciones FROM detalle_excursion AS p WHERE a.id_reservaciones = p.id_reservaciones)";
            PreparedStatement ps = accesoDB.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                reservacion = new Reservaciones();
                lugares = new Lugares();
                lugares.setIdLugares(rs.getInt("id_lugar"));
                lugares.setNombrelugar(rs.getString("nombre_lugar"));
                cliente = new Cliente();
                cliente.setDui_cliente(rs.getString("dui_cliente"));
                cliente.setNombre_cliente(rs.getString("nombre_cliente"));
                cliente.setApellido_cliente(rs.getString("apellido_cliente"));

                reservacion.setCliente(cliente);
                reservacion.setLugares(lugares);
                reservacion.setId_reservaciones(rs.getInt("id_reservaciones"));
                reservacion.setDui_cliente(rs.getString("dui_cliente"));
                reservacion.setId_lugar(rs.getInt("id_lugar"));
                reservacion.setMonto(rs.getFloat("monto"));
                reservacion.setHora(rs.getString("hora"));
                reservacion.setFecha(rs.getString("fecha"));
                reservacion.setDireccion_encuentro(rs.getString("direccion_encuentro"));

                lista_reservacion2.add(reservacion);
            }
            Conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return lista_reservacion2;
    }
    
    /*-------------------PARA LA CONSULTAS -----------------------------------*
    public List<Cliente> consulta_Cliente() throws SQLException {
        listaReservacion3 = new ArrayList<>();

        ResultSet rs = null;
        try {
            conexion = new Conexion();
            Connection accesoDB = conexion.getConexion();
            String sql = "SELECT\n"
                    + "cliente.dui_cliente,\n"
                    + "cliente.nombre_cliente,\n"
                    + "cliente.telefono_cliente,\n"
                    + "reservaciones.hora,\n"
                    + "reservaciones.fecha,\n"
                    + "reservaciones.direccion_encuentro,\n"
                    + "reservaciones.monto\n"
                    + "FROM\n"
                    + "cliente\n"
                    + "INNER JOIN reservaciones ON reservaciones.dui_cliente = cliente.dui_cliente";
            PreparedStatement ps = accesoDB.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                cliente = new Cliente();
                reservaciones = new Reservaciones();
                reservaciones.setHora(rs.getString("hora"));
                reservaciones.setFecha(rs.getString("fecha"));
                reservaciones.setDireccion_encuentro(rs.getString("direccion_encuentro"));
                reservaciones.setMonto(rs.getFloat("monto"));

                cliente.setDui_cliente(rs.getString("dui_cliente"));
                cliente.setNombre_cliente(rs.getString("nombre_cliente"));
                cliente.setApellido_cliente(rs.getString("apellido_cliente"));
                cliente.setTelefono_cliente(rs.getString("telefono_cliente"));

                lista_Cliente.add(cliente);
            }
            Conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return lista_Cliente;
    }*/
}
