/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import interfaces.InterfaceLugares;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.conexion.Conexion;
import modelo.entidades.Clasificacion;
import modelo.entidades.Lugares;

/**
 *
 * @author CRUZ
 */
public class DAOLugares implements InterfaceLugares {

    public Conexion conexion;
    private Lugares lugares;
    private Clasificacion clasificacion;
    private List<Lugares> listaLugares;

    public DAOLugares() {
    }

    public DAOLugares(Lugares lugares) {
        conexion = new Conexion();
        this.lugares = lugares;
    }

    @Override
    public Lugares obtener_Lugares() throws SQLException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        try {

            Statement st = this.conexion.getConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT*FROM lugares\n"
                    + "WHERE lugares.id_lugar = '" + lugares.getIdLugares() + "'");
            lugares = new Lugares();
            while (rs.next()) {
                lugares.setIdLugares(Integer.parseInt(rs.getString("id_lugar")));
                lugares.setNombrelugar(rs.getString("nombre_lugar"));
                lugares.setDescripcion(rs.getString("descripcion"));
//                lugares.setIdclasificacion(Integer.parseInt(rs.getString("id_clasificacion")));
                lugares.setDireccion(rs.getString("direccion_lugar"));
            }
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return lugares;
    }

    @Override
    public void insertar_Lugares() throws SQLException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        try {
            Statement st = this.conexion.getConexion().createStatement();
            String sql = "INSERT INTO lugares(id_lugar, nombre_lugar, descripcion, id_clasificacion, direccion_lugar) \n"
                    + "VALUES (\n"
                    + "'" + lugares.getIdLugares() + "',\n"
                    + "'" + lugares.getNombrelugar() + "',\n"
                    + "'" + lugares.getDescripcion() + "',\n"
                    + "'" + lugares.getIdclasificacion().getIdClasificacion() + "',\n"
                    + "'" + lugares.getDireccion() + "')";
            st.executeUpdate(sql);
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void modificar_Lugares() throws SQLException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        try {
            Statement st = this.conexion.getConexion().createStatement();
            st.executeUpdate("UPDATE lugares\n"
                    + "SET nombre_lugar='" + lugares.getNombrelugar() + "',\n"
                    + "descripcion='" + lugares.getDescripcion() + "',\n"
                    + "id_clasificacion='" + lugares.getIdclasificacion().getIdClasificacion() + "',\n"
                    + "direccion_lugar='" + lugares.getDireccion() + "'\n"
                    + "WHERE (id_lugar='"+lugares.getIdLugares()+"')");
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void elimininar_Lugares() throws SQLException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        try {
            Statement st = this.conexion.getConexion().createStatement();
            st.executeUpdate("DELETE FROM lugares WHERE (id_lugar='" + lugares.getIdLugares() + "')");
            conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public List<Lugares> mostrar_Lugares() throws SQLException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        listaLugares = new ArrayList<>();
        ResultSet rs = null;
        try {
            conexion = new Conexion();
            Connection accesoDB = conexion.getConexion();
            String sql = "SELECT\n"
                    + "clasificacion.nombre_clasificacion,\n"
                    + "lugares.nombre_lugar,\n"
                    + "lugares.descripcion,\n"
                    + "lugares.direccion_lugar,\n"
                    + "lugares.id_lugar,\n"
                    + "lugares.id_clasificacion\n"
                    + "FROM\n"
                    + "clasificacion\n"
                    + "INNER JOIN lugares ON lugares.id_clasificacion = clasificacion.id_clasificacion";
            PreparedStatement ps = accesoDB.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                lugares = new Lugares();
                clasificacion = new Clasificacion();
                clasificacion.setNombreClas(rs.getString("nombre_clasificacion"));
                
                lugares.setIdclasificacion(clasificacion);
                lugares.setIdLugares(Integer.parseInt(rs.getString("id_lugar")));
                lugares.setNombrelugar(rs.getString("nombre_lugar"));
                lugares.setDescripcion(rs.getString("descripcion"));
                lugares.setDireccion(rs.getString("direccion_lugar"));

                listaLugares.add(lugares);
            }
            Conexion.cerrarConexiones();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return listaLugares;
        
    }

}
