/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import modelo.conexion.Conexion;
import modelo.entidades.Administrador;

/**
 *
 * @author CRUZ
 */
public class AdministradorDAO {

    public Conexion conexion;
    public PreparedStatement ps;
    public ResultSet rs;
    //Administrador admin;

    public Administrador validar(String usuario, String contrasena_admin) {
        Administrador ad = new Administrador();
        String sql = "SELECT* FROM administrador WHERE dui_administrador= ? AND contrasena_admin= ? ";
        try {
            conexion = new Conexion();
            Connection accesoDB = conexion.getConexion();
            PreparedStatement ps = accesoDB.prepareStatement(sql);
            ps.setString(1, usuario);
            ps.setString(2, contrasena_admin);
            rs = ps.executeQuery();
            while (rs.next()) {
                ad.setDui(rs.getString("dui_administrador"));
                ad.setNombre(rs.getString("nombre_admin"));
                ad.setContra(rs.getString("contrasena_admin"));
            }
        } catch (Exception e) {
        }
        return ad;
    }
}
