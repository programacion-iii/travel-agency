/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.entidades;

/**
 *
 * @author BLADIMIR
 */
public class Cliente {

    private String dui_cliente;
    private String nombre_cliente;
    private String apellido_cliente;
    private String contraseña_cliente;
    private String telefono_cliente;
    private String correo_cliente;

    public Cliente() {
    }

    public Cliente(String dui_cliente, String nombre_cliente, String apellido_cliente, String contraseña_cliente, String telefono_cliente, String correo_cliente) {
        this.dui_cliente = dui_cliente;
        this.nombre_cliente = nombre_cliente;
        this.apellido_cliente = apellido_cliente;
        this.contraseña_cliente = contraseña_cliente;
        this.telefono_cliente = telefono_cliente;
        this.correo_cliente = correo_cliente;
    }

    public String getDui_cliente() {
        return dui_cliente;
    }

    public void setDui_cliente(String dui_cliente) {
        this.dui_cliente = dui_cliente;
    }

    public String getNombre_cliente() {
        return nombre_cliente;
    }

    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    public String getApellido_cliente() {
        return apellido_cliente;
    }

    public void setApellido_cliente(String apellido_cliente) {
        this.apellido_cliente = apellido_cliente;
    }

    public String getContraseña_cliente() {
        return contraseña_cliente;
    }

    public void setContraseña_cliente(String contraseña_cliente) {
        this.contraseña_cliente = contraseña_cliente;
    }

    public String getTelefono_cliente() {
        return telefono_cliente;
    }

    public void setTelefono_cliente(String telefono_cliente) {
        this.telefono_cliente = telefono_cliente;
    }

    public String getCorreo_cliente() {
        return correo_cliente;
    }

    public void setCorreo_cliente(String correo_cliente) {
        this.correo_cliente = correo_cliente;
    }

}
