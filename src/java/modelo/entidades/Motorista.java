/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.entidades;

/**
 *
 * @author pc
 */
public class Motorista {

    private String dui_motorista;
    private String nombre_motorista;
    private String apellido_motorista;
    private String telefono;
    private String direccion_motorista;
    private Boolean disponibilidad_motorista;

    public Motorista() {
    }

    public Motorista(String dui_motorista, String nombre_motorista, String apellido_motorista, String telefono, String direccion_motorista, Boolean disponibilidad_motorista) {
        this.dui_motorista = dui_motorista;
        this.nombre_motorista = nombre_motorista;
        this.apellido_motorista = apellido_motorista;
        this.telefono = telefono;
        this.direccion_motorista = direccion_motorista;
        this.disponibilidad_motorista = disponibilidad_motorista;
    }

    public String getDui_motorista() {
        return dui_motorista;
    }

    public void setDui_motorista(String dui_motorista) {
        this.dui_motorista = dui_motorista;
    }

    public String getNombre_motorista() {
        return nombre_motorista;
    }

    public void setNombre_motorista(String nombre_motorista) {
        this.nombre_motorista = nombre_motorista;
    }

    public String getApellido_motorista() {
        return apellido_motorista;
    }

    public void setApellido_motorista(String apellido_motorista) {
        this.apellido_motorista = apellido_motorista;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion_motorista() {
        return direccion_motorista;
    }

    public void setDireccion_motorista(String direccion_motorista) {
        this.direccion_motorista = direccion_motorista;
    }

    public Boolean getDisponibilidad_motorista() {
        return disponibilidad_motorista;
    }

    public void setDisponibilidad_motorista(Boolean disponibilidad_motorista) {
        this.disponibilidad_motorista = disponibilidad_motorista;
    }
}
