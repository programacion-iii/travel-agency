/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.entidades;

/**
 *
 * @author BLADIMIR
 */
public class Reservaciones {

    private int id_reservaciones;
    private String dui_cliente;
    private int id_lugar;
    private float monto;
    private String hora;
    private String fecha;
    private String direccion_encuentro;

    private Lugares lugares;
    private Cliente cliente;
    private Motorista motorista;
    private DetalleExcursion detalle;
    

    public Reservaciones() {
    }

    public Reservaciones(int id_reservaciones, String dui_cliente, int id_lugar, float monto, String hora, String fecha, String direccion_encuentro, Lugares lugares, Cliente cliente) {
        this.id_reservaciones = id_reservaciones;
        this.dui_cliente = dui_cliente;
        this.id_lugar = id_lugar;
        this.monto = monto;
        this.hora = hora;
        this.fecha = fecha;
        this.direccion_encuentro = direccion_encuentro;
        this.lugares = lugares;
        this.cliente = cliente;
    }

    public int getId_reservaciones() {
        return id_reservaciones;
    }

    public void setId_reservaciones(int id_reservaciones) {
        this.id_reservaciones = id_reservaciones;
    }

    public String getDui_cliente() {
        return dui_cliente;
    }

    public void setDui_cliente(String dui_cliente) {
        this.dui_cliente = dui_cliente;
    }

    public int getId_lugar() {
        return id_lugar;
    }

    public void setId_lugar(int id_lugar) {
        this.id_lugar = id_lugar;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDireccion_encuentro() {
        return direccion_encuentro;
    }

    public void setDireccion_encuentro(String direccion_encuentro) {
        this.direccion_encuentro = direccion_encuentro;
    }

    public Lugares getLugares() {
        return lugares;
    }

    public void setLugares(Lugares lugares) {
        this.lugares = lugares;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

}
