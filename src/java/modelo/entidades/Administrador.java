/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.entidades;

/**
 *
 * @author CRUZ
 */
public class Administrador {

    private String dui;
    private String nombre;
    private String apellido;
    private String contra;

    public Administrador() {
    }

    public Administrador(String dui, String nombre, String apellido, String contra) {
        this.dui = dui;
        this.nombre = nombre;
        this.apellido = apellido;
        this.contra = contra;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getContra() {
        return contra;
    }

    public void setContra(String contra) {
        this.contra = contra;
    }

}
