/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.entidades;

/**
 *
 * @author CRUZ
 */
public class Lugares {
    private int idLugares;
    private String nombrelugar;
    private String direccion;
    private String descripcion;
    private Clasificacion idclasificacion;
    

    public Lugares() {
    }

    public Lugares(int idLugares, String nombrelugar, String direccion, String descripcion, Clasificacion idclasificacion) {
        this.idLugares = idLugares;
        this.nombrelugar = nombrelugar;
        this.direccion = direccion;
        this.descripcion = descripcion;
        this.idclasificacion = idclasificacion;
    }

  

    public int getIdLugares() {
        return idLugares;
    }

    public void setIdLugares(int idLugares) {
        this.idLugares = idLugares;
    }

    public String getNombrelugar() {
        return nombrelugar;
    }

    public void setNombrelugar(String nombrelugar) {
        this.nombrelugar = nombrelugar;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Clasificacion getIdclasificacion() {
        return idclasificacion;
    }

    public void setIdclasificacion(Clasificacion idclasificacion) {
        this.idclasificacion = idclasificacion;
    }

    
}
