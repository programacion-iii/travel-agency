/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.entidades;

/**
 *
 * @author CRUZ
 */
public class Clasificacion {
    private int idClasificacion;
    private String NombreClas;

    public Clasificacion() {
    }

    public Clasificacion(int idClasificacion, String NombreClas) {
        this.idClasificacion = idClasificacion;
        this.NombreClas = NombreClas;
    }

    public int getIdClasificacion() {
        return idClasificacion;
    }

    public void setIdClasificacion(int idClasificacion) {
        this.idClasificacion = idClasificacion;
    }

    public String getNombreClas() {
        return NombreClas;
    }

    public void setNombreClas(String NombreClas) {
        this.NombreClas = NombreClas;
    }
    
    
}
