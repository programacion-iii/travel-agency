/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo.entidades;

/**
 *
 * @author EQUIPO
 */
public class DetalleExcursion {
    private int iddetalle;
    private Reservaciones idreservacion;
    private Transporte placa;
    private Motorista duimotorista;
    private boolean confirmacion;

    public DetalleExcursion() {
    }

    public DetalleExcursion(int iddetalle, Reservaciones idreservacion, Transporte placa, Motorista duimotorista, boolean confirmacion) {
        this.iddetalle = iddetalle;
        this.idreservacion = idreservacion;
        this.placa = placa;
        this.duimotorista = duimotorista;
        this.confirmacion = confirmacion;
    }

    public int getIddetalle() {
        return iddetalle;
    }

    public void setIddetalle(int iddetalle) {
        this.iddetalle = iddetalle;
    }

    public Reservaciones getIdreservacion() {
        return idreservacion;
    }

    public void setIdreservacion(Reservaciones idreservacion) {
        this.idreservacion = idreservacion;
    }

    public Transporte getPlaca() {
        return placa;
    }

    public void setPlaca(Transporte placa) {
        this.placa = placa;
    }

    public Motorista getDuimotorista() {
        return duimotorista;
    }

    public void setDuimotorista(Motorista duimotorista) {
        this.duimotorista = duimotorista;
    }

    public boolean isConfirmacion() {
        return confirmacion;
    }

    public void setConfirmacion(boolean confirmacion) {
        this.confirmacion = confirmacion;
    }

   
    
    
}
