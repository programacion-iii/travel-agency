/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.sql.SQLException;
import java.util.List;
import modelo.entidades.Cliente;

/**
 *
 * @author BLADIMIR
 */
public interface Interface_Cliente {

    public Cliente obtener_Cliente() throws SQLException;

    public void insertar_Cliente() throws SQLException;

    public void modificar_Cliente() throws SQLException;

    public void elimininar_Cliente() throws SQLException;

    public List<Cliente> mostrar_Cliente() throws SQLException;

}
