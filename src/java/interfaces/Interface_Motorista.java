/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.sql.SQLException;
import java.util.List;
import modelo.entidades.Motorista;

/**
 *
 * @author pc
 */
public interface Interface_Motorista {

    public Motorista obtener_Motorista() throws SQLException;

    public void insertar_Motorista() throws SQLException;

    public void modificar_Motorista() throws SQLException;

    public void elimininar_Motorista() throws SQLException;

    public List<Motorista> mostrar_Motorista() throws SQLException;

}
