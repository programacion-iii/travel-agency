/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.sql.SQLException;
import java.util.List;
import modelo.entidades.Reservaciones;

/**
 *
 * @author BLADIMIR
 */
public interface Interface_Reservaciones {
    
    public Reservaciones obtener_Reservacion() throws SQLException;

    public void insertar_Reservacion() throws SQLException;

    public void modificar_Reservacion() throws SQLException;

    public void elimininar_Reservacion() throws SQLException;

    public List<Reservaciones> mostrar_Reservacion() throws SQLException;
    
}
