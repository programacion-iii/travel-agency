/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package interfaces;

import java.sql.SQLException;
import java.util.List;
import modelo.entidades.DetalleExcursion;

/**
 *
 * @author EQUIPO
 */
public interface InterfaceDetalleExcursion {
    
     public DetalleExcursion obtenerDetalle() throws SQLException;

    public void insertarDetalle() throws SQLException;

    public void modificarDetalle() throws SQLException;

    public void eliminarDetalle() throws SQLException;

    public List<DetalleExcursion> mostrarDetalle() throws SQLException;
}
