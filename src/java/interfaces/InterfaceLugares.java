/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.sql.SQLException;
import java.util.List;
import modelo.entidades.Lugares;

/**
 *
 * @author CRUZ
 */
public interface InterfaceLugares {

    public Lugares obtener_Lugares() throws SQLException;

    public void insertar_Lugares() throws SQLException;

    public void modificar_Lugares() throws SQLException;

    public void elimininar_Lugares() throws SQLException;

    public List<Lugares> mostrar_Lugares() throws SQLException;
}
