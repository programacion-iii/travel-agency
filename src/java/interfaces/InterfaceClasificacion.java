/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.sql.SQLException;
import java.util.List;
import modelo.entidades.Clasificacion;

/**
 *
 * @author CRUZ
 */
public interface InterfaceClasificacion {
    public List<Clasificacion> mostrar_Clasificacion() throws SQLException;
}
