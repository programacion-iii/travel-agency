/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.dao.DAO_Reservaciones;
import modelo.dao.Dao_Cliente;
import modelo.entidades.Cliente;
import modelo.entidades.Lugares;
import modelo.entidades.Reservaciones;

/**
 *
 * @author BLADIMIR
 */
public class Servlet_Reservaciones extends HttpServlet {

    private DAO_Reservaciones daoReservaciones = new DAO_Reservaciones();
    private Reservaciones reservacion = new Reservaciones();
    private Reservaciones modificarreservacion;
    private Cliente cliente = new Cliente();
    private Lugares lugar = new Lugares();
    
       private List<Reservaciones> lista_reservaciones_tabla;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        
        DAO_Reservaciones daoReservaciones = new DAO_Reservaciones();
        try {
           
            lista_reservaciones_tabla = daoReservaciones.mostrar_Reservacion();
            request.setAttribute("lista_all_reservaciones", lista_reservaciones_tabla);//Lista
            
          
            request.getRequestDispatcher("Consultas/NO_FILTRO_Reservaciones_de_clientes.jsp").forward(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Servlet_Reservaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("borrar") != null) {
            try {
                reservacion.setId_reservaciones(Integer.parseInt(request.getParameter("borrar").trim()));
                if (reservacion != null) {
                    daoReservaciones = new DAO_Reservaciones(reservacion);
                    daoReservaciones.elimininar_Reservacion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(Servlet_Cliente.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println(request.getParameter("borrar"));
            request.getRequestDispatcher("CRUD_Reservaciones.jsp").forward(request, response);
        }
        /*--------------------------------------------------------------------*/
        if ((request.getParameter("registrar") != null) || (request.getParameter("modificar") != null)) {
            try {
                cliente.setDui_cliente(request.getParameter("id_combo_cliente").trim());
                lugar.setIdLugares(Integer.parseInt(request.getParameter("id_combo_lugar").trim()));

                reservacion.setId_reservaciones(Integer.parseInt(request.getParameter("id_reservaciones").trim()));
                reservacion.setCliente(cliente);
                reservacion.setLugares(lugar);
                reservacion.setHora(request.getParameter("hora").trim());
                reservacion.setFecha(request.getParameter("fecha").trim());
                reservacion.setMonto(Float.parseFloat(request.getParameter("monto").trim()));
                reservacion.setDireccion_encuentro(request.getParameter("direccion_encuentro").trim());

                daoReservaciones = new DAO_Reservaciones(reservacion);
                
                if (reservacion != null) {
                    daoReservaciones = new DAO_Reservaciones(reservacion);
                    if (request.getParameter("registrar") != null) {
                        daoReservaciones.insertar_Reservacion();
                    } else {
                        daoReservaciones.modificar_Reservacion();
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(Servlet_Cliente.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.getRequestDispatcher("CRUD_Reservaciones.jsp").forward(request, response);
        }
        /*--------------------------------------------------------------------*/
        if (request.getParameter("cancelar") != null) {
            request.getRequestDispatcher("CRUD_Reservaciones.jsp").forward(request, response);
        }
        /*--------------------------------------------------------------------*/
        if (request.getParameter("editar") != null) {
            reservacion.setId_reservaciones(Integer.parseInt(request.getParameter("editar").trim()));
            if (reservacion != null) {
                try {
                    daoReservaciones = new DAO_Reservaciones(reservacion);
                    modificarreservacion = daoReservaciones.obtener_Reservacion();
                    request.setAttribute("modificar", modificarreservacion);
                    request.getRequestDispatcher("CRUD_Reservaciones.jsp").forward(request, response);
                } catch (SQLException ex) {
                    Logger.getLogger(Servlet_Cliente.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        //processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
