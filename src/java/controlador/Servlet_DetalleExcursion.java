/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.dao.DAODetalleExcursion;
import modelo.entidades.DetalleExcursion;
import modelo.entidades.Motorista;
import modelo.entidades.Reservaciones;
import modelo.entidades.Transporte;

/**
 *
 * @author EQUIPO
 */
public class Servlet_DetalleExcursion extends HttpServlet {
    
    DAODetalleExcursion daodetalle = new DAODetalleExcursion();
    DetalleExcursion detalle = new DetalleExcursion();
    Motorista motorista = new Motorista();
    Reservaciones reserva = new Reservaciones();
    Transporte transporte = new Transporte();
    DetalleExcursion modificadetalle = new DetalleExcursion();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Servlet_DetalleExcursion</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Servlet_DetalleExcursion at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        if (request.getParameter("borrar") != null) {
            try {
                detalle.setIddetalle(Integer.parseInt(request.getParameter("borrar").trim()));
                if (detalle != null) {
                    daodetalle = new DAODetalleExcursion(detalle);
                    daodetalle.eliminarDetalle();
                }
            } catch (SQLException ex) {
                Logger.getLogger(Servlet_Lugares.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println(request.getParameter("borrar"));
            request.getRequestDispatcher("CRUDetalleExcursion.jsp").forward(request, response);
        }
        /*--------------------------------------------------------------------*/
        if ((request.getParameter("registrar") != null) || (request.getParameter("modificar") != null)) {
            try {
                motorista.setDui_motorista(request.getParameter("id_motorista").trim());
                reserva.setId_reservaciones(Integer.parseInt(request.getParameter("id_reserva").trim()));
                transporte.setPlaca(request.getParameter("id_transporte").trim());
                
//                detalle.setIddetalle(Integer.parseInt(request.getParameter("id_detalle")));
                detalle.setConfirmacion(Boolean.parseBoolean(request.getParameter("confirma").trim()));
                detalle.setDuimotorista(motorista);
                detalle.setIdreservacion(reserva);
                detalle.setPlaca(transporte);
                

                if (detalle != null) {
                    daodetalle= new DAODetalleExcursion(detalle);
                    if (request.getParameter("registrar") != null) {
                        daodetalle.insertarDetalle();
                    } else {
                        daodetalle.modificarDetalle();
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(Servlet_Lugares.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.getRequestDispatcher("CRUDetalleExcursion.jsp").forward(request, response);
        }
        /*--------------------------------------------------------------------*/
        if (request.getParameter("cancelar") != null) {
            request.getRequestDispatcher("CRUDetalleExcursion.jsp").forward(request, response);
        }
        /*--------------------------------------------------------------------*/
        if (request.getParameter("editar") != null) {
            detalle.setIddetalle(Integer.parseInt(request.getParameter("editar").trim()));
            if (detalle != null) {
                try {
                    daodetalle = new DAODetalleExcursion(detalle);
                    modificadetalle = daodetalle.obtenerDetalle();
                    request.setAttribute("modificar", modificadetalle);
                    request.getRequestDispatcher("CRUDetalleExcursion.jsp").forward(request, response);
                } catch (SQLException ex) {
                    Logger.getLogger(Servlet_Lugares.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
