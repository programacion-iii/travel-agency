/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.dao.DAOLugares;
import modelo.entidades.Clasificacion;
import modelo.entidades.Lugares;

/**
 *
 * @author CRUZ
 */
public class Servlet_Lugares extends HttpServlet {

    DAOLugares daolugares = new DAOLugares();
    Lugares lugares = new Lugares();
    Clasificacion clasif = new Clasificacion();
    Lugares modificarLugares = new Lugares();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Servlet_Lugares</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Servlet_Lugares at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        if (request.getParameter("borrar") != null) {
            try {
                lugares.setIdLugares(Integer.parseInt(request.getParameter("borrar").trim()));
                if (lugares != null) {
                    daolugares = new DAOLugares(lugares);
                    daolugares.elimininar_Lugares();
                }
            } catch (SQLException ex) {
                Logger.getLogger(Servlet_Lugares.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println(request.getParameter("borrar"));
            request.getRequestDispatcher("CRUDLugares.jsp").forward(request, response);
        }
        /*--------------------------------------------------------------------*/
        if ((request.getParameter("registrar") != null) || (request.getParameter("modificar") != null)) {
            try {
                clasif.setIdClasificacion(Integer.parseInt(request.getParameter("combClasi").trim()));
                lugares.setIdLugares(Integer.parseInt(request.getParameter("id_lugares").trim()));
                lugares.setNombrelugar(request.getParameter("nombre_lugar").trim());
                lugares.setDescripcion(request.getParameter("descripcion_lugar").trim());
                lugares.setIdclasificacion(clasif);
                lugares.setDireccion(request.getParameter("direccion_lugar").trim());

                if (lugares != null) {
                    daolugares = new DAOLugares(lugares);
                    if (request.getParameter("registrar") != null) {
                        daolugares.insertar_Lugares();
                    } else {
                        daolugares.modificar_Lugares();
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(Servlet_Lugares.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.getRequestDispatcher("CRUDLugares.jsp").forward(request, response);
        }
        /*--------------------------------------------------------------------*/
        if (request.getParameter("cancelar") != null) {
            request.getRequestDispatcher("CRUDLugares.jsp").forward(request, response);
        }
        /*--------------------------------------------------------------------*/
        if (request.getParameter("editar") != null) {
            lugares.setIdLugares(Integer.parseInt(request.getParameter("editar").trim()));
            if (lugares != null) {
                try {
                    daolugares = new DAOLugares(lugares);
                    modificarLugares = daolugares.obtener_Lugares();
                    request.setAttribute("modificar", modificarLugares);
                    request.getRequestDispatcher("CRUDLugares.jsp").forward(request, response);
                } catch (SQLException ex) {
                    Logger.getLogger(Servlet_Lugares.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
