<%-- 
    Document   : CRUD_Reservaciones
    Created on : 01-07-2021, 11:13:30 PM
    Author     : BLADIMIR
--%>

<%@page import="modelo.entidades.Lugares"%>
<%@page import="modelo.entidades.Cliente"%>
<%@page import="modelo.entidades.Reservaciones"%>
<%@page import="modelo.dao.DAO_Reservaciones"%>
<%@page import="modelo.dao.DAOLugares"%>
<%@page import="modelo.dao.Dao_Cliente"%>
<!DOCTYPE html>
<%
    Dao_Cliente daoCLIENTE = new Dao_Cliente();
    DAOLugares daoLUGARES = new DAOLugares();
    DAO_Reservaciones daoReservaciones = new DAO_Reservaciones();

    Reservaciones modificar = (Reservaciones) request.getAttribute("modificar");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>CRUD RESERVACIONES</title>
        <link href="recursos/combo/select_css.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="recursos/css/core.css">    
        <link rel="stylesheet" type="text/css" href="recursos/css/bootstrap.css">
        <link href="recursos/css/table.css" rel="stylesheet">
    </head>
    <body>
        <script>
            function mayus(e) {
                e.value = e.value.toUpperCase();
            }
        </script>
        <%--------------------------ENCABEZADO GRIS---------------------------%>
        <div class="message-bar">
            <span class="pos">
                <%=(modificar == null) ? "Registro" : "Modificacion"%> de Reservaciones
            </span>
        </div>
        <%--------------------------------------------------------------------%>
        <jsp:include page="menu_secundario_admin.html"></jsp:include>
            <div class="container">
                <div class="col-lg-12">
                    <div align="center">
                        <h1><%=(modificar == null) ? "NUEVA" : "EDITAR"%> RESERVACION</h1><br>
                </div>
                <%------------------------  FORMULARIO 1 ---------------------%>
                <form  action="Servlet_Reservaciones" method="post">
                    <%-----------------------------------------------------------------------------------------------------------------------%>
                    <input type="text" name="id_reservacioness" value="<%=modificar != null ? modificar.getId_reservaciones() : ""%>" style="display:none">                    
                    <%-----------------------------------------------------------------------------------------------------------------------%>
                    <div class="inputform ">
                        <label>ID: </label>
                        <input  value="<%=(modificar == null) ? "" : modificar.getId_reservaciones()%>" 
                                name="id_reservaciones"
                                type="text" 
                                class="input"
                                placeholder="" 
                                required="true"
                                pattern="[0-9]{1,200000}">
                        <span class="form_hint"></span>
                    </div>

                    <div  class="inputform">
                        <label>Cliente:</label>
                        <select id="id_combo_cliente" name="id_combo_cliente" class="select-css">
                            <% for (Cliente x : daoCLIENTE.mostrar_Cliente()) {
                                    if (modificar != null) {
                                if (x.getDui_cliente().equalsIgnoreCase(modificar.getDui_cliente())) {%>
                            <option selected value="<%=x.getDui_cliente()%>"><%=x.getNombre_cliente()+" "+x.getApellido_cliente() %></option>
                            <%} else {%>
                            <option value="<%=x.getDui_cliente()%>"><%=x.getNombre_cliente()+" "+x.getApellido_cliente()%></option>
                            <%}%>
                            <%} else {%>
                            <option value="<%=x.getDui_cliente()%>"><%=x.getNombre_cliente()+" "+x.getApellido_cliente() %></option>
                            <%}
                                }%>
                        </select>
                    </div>

                    <div  class="inputform">
                        <label>Lugar:</label>
                        <select id="id_combo_lugar" name="id_combo_lugar" class="select-css">
                            <% for (Lugares x : daoLUGARES.mostrar_Lugares()) {
                                    if (modificar != null) {
                                        if (x.getIdLugares()== modificar.getId_lugar()) {%>
                            <option selected value="<%=x.getIdLugares()%>"><%=x.getNombrelugar()%></option>
                            <%} else {%>
                            <option value="<%=x.getIdLugares()%>"><%=x.getNombrelugar()%></option>
                            <%}%>
                            <%} else {%>
                            <option value="<%=x.getIdLugares()%>"><%=x.getNombrelugar()%></option>
                            <%}
                                }%>
                        </select>
                    </div>

                    <div class="inputform ">
                        <label>Monto : </label>
                        <input  value="<%=(modificar == null) ? "" : modificar.getMonto()%>" 
                                name="monto"
                                type="text" 
                                class="input"
                                placeholder="11.32" 
                                required="true"
                                pattern="[0.0-9.0]{1,200000}">
                        <span class="form_hint"></span>
                    </div>

                    <div class="inputform ">
                        <label>Hora: </label>
                        <input  value="<%=(modificar == null) ? "" : modificar.getHora()%>" 
                                name="hora"
                                type="time" 
                                class="input"
                                placeholder="" 
                                required="true">
                        <span class="form_hint"></span>
                    </div>

                    <div class="inputform ">
                        <label>Fecha: </label>
                        <input  value="<%=(modificar == null) ? "" : modificar.getFecha()%>" 
                                name="fecha"
                                type="date" 
                                class="input"
                                placeholder="" 
                                required="true">
                        <span class="form_hint"></span>
                    </div>

                    <div class="inputform ">
                        <label>Direccion de partida: </label>
                        <input  value="<%=(modificar == null) ? "" : modificar.getDireccion_encuentro()%>" 
                                name="direccion_encuentro"
                                type="text" 
                                class="input"
                                placeholder="" 
                                required="true"
                                pattern="[a-�-z A-�-Z]{2,254}"
                                onkeyup="mayus(this);">
                        <span class="form_hint"></span>
                    </div>

                    <%------------------------ TABLA 1 -----------------------%>
                    <table>
                        <tr>
                            <td width="48%"></td>
                            <td><input type="submit" class="submit btn btn-primary readMore roundBtn" value="<%=(modificar == null) ? "Registrar" : "Modificar"%>" name="<%=(modificar == null) ? "registrar" : "modificar"%>" ></td>
                            <td><div><input type="submit" class="submit btn btn-primary readMore roundBtn" name="cancelar"  value="Cancelar"></div></td>
                            <td width="48%"></td>
                        </tr>
                    </table>
                    <%--------------------- FIN TABLA 1 -----------------------%>     
                </form>
                <%--------------------  FIN FORMULARIO 1 ---------------------%>

                <%--------------------SEPARADOR DE FORMULARIOS---------------------%>

                <%------------------------FORMULARIO 2------------------------%>
                <form action="Servlet_Reservaciones" method="post">
                    <div style="<%=(daoReservaciones.mostrar_Reservacion().isEmpty()) ? "display:none" : "display:block"%>;
                         overflow: auto; max-height: 300px;">
                        <table class="table" >
                            <thead>
                                <tr>
                                    <th>ID Reservacion</th>
                                    <th>Cliente</th>   
                                    <th>Lugar</th>                                   
                                    <th>Hora</th>
                                    <th>Fecha</th>
                                    <th>Monto</th>
                                    <th>Direccion Partida</th>                                                     
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%for (Reservaciones reservacion : daoReservaciones.mostrar_Reservacion()) {%>
                                <tr>
                                    <td><%= reservacion.getId_reservaciones()%></td>
                                    <td><%= reservacion.getCliente().getNombre_cliente()%></td>
                                    <td><%= reservacion.getLugares().getNombrelugar()%></td>
                                    <td><%= reservacion.getHora()%></td>
                                    <td><%= reservacion.getFecha()%></td>
                                    <td><%= reservacion.getMonto()%></td>
                                    <td><%= reservacion.getDireccion_encuentro()%></td>

                                    <td><div align="right"><button class="btn btn-primary readMore roundBtn" name="editar" value="<%=reservacion.getId_reservaciones()%>">Editar</button></div>
                                    <td><div align="right"><button class="btn btn-primary readMore roundBtn" name="borrar" value="<%=reservacion.getId_reservaciones()%>">Borrar</button></div>
                                </tr>
                                <%}%>
                            </tbody>
                        </table>
                    </div>  
                </form>                
            </div>      
        </div>
    </body>
</html>
