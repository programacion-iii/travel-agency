<%-- 
    Document   : CRUD_Cliente
    Created on : 11-19-2020, 08:08:11 PM
    Author     : BLADIMIR
--%>

<%@page import="modelo.entidades.Cliente"%>
<%@page import="modelo.dao.Dao_Cliente"%>
<!DOCTYPE html>
<%
    Dao_Cliente daoCliente = new Dao_Cliente();
    Cliente modificar = (Cliente) request.getAttribute("modificar");
%>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

        <title>CRUD CLIENTE</title>

        <link rel="stylesheet" type="text/css" href="recursos/css/core.css">
        <link rel="stylesheet" type="text/css" href="recursos/css/bootstrap.css">
        <link href="recursos/css/table.css" rel="stylesheet">

    </head>
    <body>
        <script>
            function mayus(e) {
                e.value = e.value.toUpperCase();
            }
        </script>
        <div class="message-bar">
            <span class="post"><%=(modificar == null) ? "REGISTRO" : "MODIFICACION"%> DE CLIENTE</span>
        </div> 
        <%---------------- FIN EN CABEZADO DE LA PAGINA ----------------------%>
        <jsp:include page="menu_secundario_admin.html"></jsp:include>
            <div class="container">
                <div class="col-lg-12">

                    <div align="center"><h1><%=(modificar == null) ? "Nuevo " : "Editar "%>Cliente</h1> </div>

                <%------------------- INICIO FORMULARIO 1 --------------------%>

                <form action="Servlet_Cliente" method="post">


                    <input value="<%=modificar != null ? modificar.getDui_cliente() : ""%>" type="text" name="dui"  style="display:none"><br>


                    <div class="inputform">
                        <label>DUI: </label>
                        <input value="<%=(modificar == null) ? "" : modificar.getDui_cliente()%>" type="text" class="input" name="dui_cliente" 
                               placeholder="" pattern="[0-9]{1,9}"   title="[123456789]">
                        <span class="form_hint"></span>
                    </div>


                    <div  class="inputform">
                        <label>Nombre: </label>
                        <input value="<%=(modificar == null) ? "" : modificar.getNombre_cliente()%>" type="text" class="input" name="nombre_cliente" 
                               placeholder="" required="true" pattern="[a-�-z A-�-Z]{2,254}" title="NOMBRE1 NOMBRE2" onkeyup="mayus(this);">
                        <span class="form_hint"></span>
                    </div>

                    <div  class="inputform">
                        <label>Apellido: </label>
                        <input value="<%=(modificar == null) ? "" : modificar.getApellido_cliente()%>" type="text" class="input" name="apellido_cliente" 
                               placeholder="" required="true"  pattern="[a-�-z A-�-Z]{2,254}" title="APELLIDO1 APELLIDO2" onkeyup="mayus(this);">
                        <span class="form_hint"></span>
                    </div>

                    <div  class="inputform">
                        <label>Contrase�a: </label>
                        <input value="<%=(modificar == null) ? "" : modificar.getContrase�a_cliente()%>" type="text" class="input" name="contra_cliente" 
                               placeholder="125_FDS" required="true"  title="######">
                        <span class="form_hint"></span>
                    </div>

                    <div  class="inputform">
                        <label>Telefono: </label>
                        <input value="<%=(modificar == null) ? "" : modificar.getTelefono_cliente()%>" type="text" class="input" name="telefono_cliente" 
                               placeholder="" required="true" pattern="[0-9]{1,9}"title="[########]">
                        <span class="form_hint"></span>
                    </div>

                    <div  class="inputform">
                        <label>Correo: </label>
                        <input value="<%=(modificar == null) ? "" : modificar.getCorreo_cliente()%>" type="text" class="input" name="correo_cliente" 
                               placeholder="" required="true"   title="[NOMBRESAPELLIDOS@GMAIL.COM]" onkeyup="mayus(this);">
                        <span class="form_hint"></span>
                    </div>

                    <%------------------- INICIO TABLA 1 ---------------------%>
                    <table>
                        <tr>
                            <td width="48%"></td>
                            <td>
                                <input value="<%=(modificar == null) ? "Registrar" : "Modificar"%>"
                                       name="<%=(modificar == null) ? "registrar" : "modificar"%>"
                                       type="submit" class="submit btn btn-primary readMore roundBtn">
                            </td>
                            <td>
                                <input value="Cancelar" 
                                       name="cancelar"
                                       type="submit" class="submit btn btn-primary readMore roundBtn">
                            </td>
                            <td width="48%"></td>
                        </tr>
                    </table>
                    <%------------------- FIN TABLA 1 ------------------------%>
                </form><br>
                <%------------------- FIN FORMULARIO 1 -----------------------%>
                <form action="Servlet_Cliente" method="post">
                    <div style="<%=(daoCliente.mostrar_Cliente().isEmpty()) ? "display:none" : "display:block"%>;
                         overflow: auto; max-height: 300px;">
                        <table class="table" >
                            <thead>
                                <tr>
                                    <th>DUI</th>
                                    <th>Nombre</th>                                   
                                    <th>Apellido</th>                                   
                                    <th>Contrase�a</th>                                   
                                    <th>Telefono</th>                                   
                                    <th>Correo</th>                                   
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%for (Cliente registro : daoCliente.mostrar_Cliente()) {%>
                                <tr>
                                    <td><%= registro.getDui_cliente()%></td>
                                    <td><%= registro.getNombre_cliente()%></td>
                                    <td><%= registro.getApellido_cliente()%></td>
                                    <td><%= registro.getContrase�a_cliente()%></td>
                                    <td><%= registro.getTelefono_cliente()%></td>
                                    <td><%= registro.getCorreo_cliente()%></td>

                                    <td><div align="right">
                                            <button class="btn btn-primary readMore roundBtn"
                                                    name="editar" value="<%=registro.getDui_cliente()%>">Editar</button>
                                        </div>
                                    <td><div align="right">
                                            <button class="btn btn-primary readMore roundBtn"                                                    
                                                    name="borrar" value="<%=registro.getDui_cliente()%>">Borrar</button>
                                        </div>
                                </tr>
                                <%}%>
                            </tbody>
                        </table>
                    </div>  
                </form>
            </div>
        </div>
    </body>
</html>
