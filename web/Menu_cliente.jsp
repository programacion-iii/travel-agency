<%-- 
    Document   : Home_cliente
    Created on : 11-21-2020, 07:47:38 PM
    Author     : BLADIMIR
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>HOME CLIENTE</title>
        <link rel="stylesheet" href="recursos/bulma/css/bulma.css">
        <link rel="stylesheet" href="recursos/bulma/css/bulma-carousel.min.css">

    <a href="recursos/bulma/js/bulma-carousel.min.js"></a>
</head>
<body>

    <section class="section">
        <div class="container">
            <!-- Start Carousel -->
            <div id="carousel-demo" class="carousel">
                <div class="item-1">
                    <!-- Slide Content -->
                </div>
                <div class="item-2">
                    <!-- Slide Content -->
                </div>
                <div class="item-3">
                    <!-- Slide Content -->
                </div>
            </div>
            <!-- End Carousel -->
        </div>
    </section>

    <script src="https://cdn.jsdelivr.net/npm/bulma-carousel@4.0.3/dist/js/bulma-carousel.min.js"></script>
    <script>
        bulmaCarousel.attach('#carousel-demo', {
            slidesToScroll: 1,
            slidesToShow: 4
        });
    </script>

<li>
    <a href="">Regístrate</a>
    <a href="">Iniciar sesión</a>
</li>
<div class="container">
    <div class="columns">
        <div class="column">
            <div class="title"> AGENCIA DE VIAJES NEPTUNO</div>
        </div>
        <div class="column">
            <img src="recursos/images/Neptune.png" width="107" height="107" alt="Neptune"/>
        </div>
    </div>
</div>

<div class="column">
    <p class="card-header">Descubre todos los lugares hermosos de el salvador.<br>
        Reserva con anticipación y ahorra en tus salidas con nuestros planes flexibles.</p>
</div>

<div class="container">
    <div class="columns">
        <div class="column">
            <figure class=" image is-square">
                <img src="recursos/images/parque.jpg" alt="parque"/>
            </figure>
            <p class="notification is-success">PARQUES NACIONALES</p>
        </div>
        <div class="column">
            <figure class="image is-square">
                <img src="recursos/images/playa.jpg" alt="playa"/>
            </figure>
            <p class="notification is-success">PLAYAS</p>
        </div>
        <div class="column">
            <figure class="image is-square">
                <img src="recursos/images/lago.jpg" alt="lago"/>
            </figure>
            <p class="notification is-success">TURICENTROS</p>
        </div>
        <div class="column">
            <figure class="image is-square">
                <img src="recursos/images/ciudades.jpg" alt="ciudades"/>
            </figure>
            <p class="notification is-success">CIUDADES</p>
        </div>
        <div class="column">
            <figure class="image is-square">
                <img src="recursos/images/volcanes y cerros.jpg" alt="volcanes y cerros"/>
            </figure>
            <p class="notification is-success">VOCANES Y CERROS</p>
        </div>
        <div class="column">
            <figure class="image is-square">
                <img src="recursos/images/ruinas.jpg" alt="ruinas"/>
            </figure>
            <p class="notification is-success">RUINAS</p>
        </div>
    </div>
    <div class="content has-text-centered">
        <p class="title is-4">Regístrate o inicia sesión ya!, para que puedas hacer tus reservaciones.</p>
    </div>
</div> 
<footer class="footer">
    <div class="content has-text-centered"></div>
    <div class="container">
        <div class="columns">
            <div class="column"><h4 class="title is-5">HOME</h4>
            </div>
            <div class="column"><p class="title is-5">DOCUMENTACION</p>
            </div>
            <div class="column"><p class="title is-5">MAS</p>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
