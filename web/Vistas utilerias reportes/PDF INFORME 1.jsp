<%-- 
    Document   : PDF INFORME 1
    Created on : 01-12-2021, 01:59:17 AM
    Author     : BLADIMIR
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="modelo.conexion.Conexion"%>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="net.sf.jasperreports.engine.*" %>
<%@page import="net.sf.jasperreports.view.JasperViewer" %>
<%@page import="javax.servlet.ServletResponse"%>
<%@page import="modelo.conexion.Conexion"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>INFORME 1</title>
    </head>
    <body>
        <%
            Conexion conexion = new Conexion();
            File reportfile = new File(application.getRealPath("Reportes/Informe 1.jasper"));
            Map<String, Object> parameter = new HashMap<String, Object>();
            byte[] bytes = JasperRunManager.runReportToPdf(reportfile.getPath(), parameter, conexion.getConexion());//creacion del informe 
            response.setContentType("application/pdf");//El informe estarà en formato pdf 
            response.setContentLength(bytes.length);
            ServletOutputStream outpoutstream = response.getOutputStream();// salida para el reporte pdf
            outpoutstream.write(bytes, 0, bytes.length); //imprime el reporte
            outpoutstream.flush(); // limpiar la salida del informe
            outpoutstream.close(); // cerrar la salida del informe
%>
    </body>
</html>
