<%-- 
    Document   : index
    Created on : 01-09-2021, 10:20:10 PM
    Author     : CRUZ
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="recursos/css/bootstrap.min.css">
        <title>Loggin</title>
    </head>
    <body >
        <div class="container mt-4 col-lg-4">
            <div class="card col-sm-10">
                <div class="card-body ">
                    <form class="form-sing" action="Validar" method="post">
                        <div class="form-group text-center">
                            <h3>VIAJES NEPTUNO</h3>
                            <img src="recursos/images/Neptune.png" width="107" height="107" alt="Neptune"/>
                            <label>Bienvenidos al sistema</label>
                        </div>
                        <div class="form-group">
                            <label>Usuario:</label>
                            <input type="text" name="txtuser" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Contraseña:</label>
                            <input type="password" name="txtpass" class="form-control">
                        </div>
                        <input type="submit" name="accion" value="Ingresar" class="btn btn-primary btn bloc" >
                       
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
