<%-- 
    Document   : CRUD_Motorista
    Created on : 11-23-2020, 10:02:10 PM
    Author     : pc
--%>

<%@page import="modelo.entidades.Motorista"%>
<%@page import="modelo.dao.DAO_Motorista"%>
<!DOCTYPE html>
<%
    DAO_Motorista daoMotorista = new DAO_Motorista();
    Motorista modificar = (Motorista) request.getAttribute("modificar");
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>CRUD PARA MOTORISTAS</title>
        <link href="recursos/combo/select_css.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="recursos/css/core.css">
        <link rel="stylesheet" type="text/css" href="recursos/css/bootstrap.css">
        <link href="recursos/css/table.css" rel="stylesheet">
    </head>
    <body>
        <script>
            function mayus(e) {
                e.value = e.value.toUpperCase();
            }
        </script>
        <div class="message-bar">
            <span class="post"><%=(modificar == null) ? "REGISTRO" : "MODIFICACION"%> DE MOTORISTAS</span>
        </div> 
        <%---------------- FIN EN CABEZADO DE LA PAGINA ----------------------%>
        <jsp:include page="menu_secundario_admin.html"></jsp:include>
            <div class="container">
                <div class="col-lg-12">

                    <div align="center"><h1><%=(modificar == null) ? "Nuevo " : "Editar "%>Motorista</h1> </div>

                <%------------------- INICIO FORMULARIO 1 --------------------%>
                <form action="Servlet_Motorista" method="post">


                    <input value="<%=modificar != null ? modificar.getDui_motorista() : ""%>" type="text" name="dui"  style="display:none"><br>


                    <div class="inputform">
                        <label>DUI: </label>
                        <input value="<%=(modificar == null) ? "" : modificar.getDui_motorista()%>" type="text" class="input" name="dui_motorista" 
                               placeholder="" required="true" pattern="[0-9]{1,9}">
                        <span class="form_hint"></span>
                    </div>


                    <div  class="inputform">
                        <label>Nombres: </label>
                        <input value="<%=(modificar == null) ? "" : modificar.getNombre_motorista()%>" type="text" class="input" name="nombre_motorista" 
                               placeholder="" required="true" pattern="[a-�-z A-�-Z]{2,254}" title="NOMBRE1 NOMBRE2" onkeyup="mayus(this);">
                        <span class="form_hint"></span>
                    </div>

                    <div  class="inputform">
                        <label>Apellidos: </label>
                        <input value="<%=(modificar == null) ? "" : modificar.getApellido_motorista()%>" type="text" class="input" name="apellido_motorista" 
                               placeholder="" required="true" pattern="[a-�-z A-�-Z]{2,254}" title="APELLIDO1 APELLIDO2" onkeyup="mayus(this);">
                        <span class="form_hint"></span>
                    </div>

                    <div  class="inputform">
                        <label>Telefono: </label>
                        <input value="<%=(modificar == null) ? "" : modificar.getTelefono()%>" type="text" class="input" name="telefono" 
                               placeholder="" required="true" pattern="[0-9]{1,9}"title="[########]">
                        <span class="form_hint"></span>
                    </div>

                    <div  class="inputform">
                        <label>Direccion: </label>
                        <input value="<%=(modificar == null) ? "" : modificar.getDireccion_motorista()%>" type="text" class="input" name="direccion_motorista" 
                               placeholder="" required="true" onkeyup="mayus(this);">
                        <span class="form_hint"></span>
                    </div>

                    <div class="inputform">
                        <label>Estado Vehiculo: </label>
                        <select id="id_combo_categorias" name="estado_motorista" class="select-css" required="true">
                            <% if (modificar != null) {
                                    if (modificar.getDisponibilidad_motorista()) {%>
                            <option selected value="TRUE">ACTIVO</option>
                            <option value="FALSE">INACTIVO</option>
                            <%} else {%>
                            <option selected value="FALSE">INACTIVO</option>
                            <option value="TRUE">ACTIVO</option>
                            <%}
                            } else {%>
                            <option value="TRUE">ACTIVO</option>
                            <option value="FALSE">INACTIVO</option>
                            <%}%>
                        </select>
                    </div> 

                    <%------------------- INICIO TABLA 1 ---------------------%>
                    <table>
                        <tr>
                            <td width="48%"></td>
                            <td>
                                <input value="<%=(modificar == null) ? "Registrar" : "Modificar"%>"
                                       name="<%=(modificar == null) ? "registrar" : "modificar"%>"
                                       type="submit" class="submit btn btn-primary readMore roundBtn">
                            </td>
                            <td>
                                <input value="Cancelar" 
                                       name="cancelar"
                                       type="submit" class="submit btn btn-primary readMore roundBtn">
                            </td>
                            <td width="48%"></td>
                        </tr>
                    </table>
                    <%------------------- FIN TABLA 1 ------------------------%>
                </form><br>
                <%------------------- FIN FORMULARIO 1 -----------------------%>
                <form action="Servlet_Motorista" method="post">
                    <div style="<%=(daoMotorista.mostrar_Motorista().isEmpty()) ? "display:none" : "display:block"%>;
                         overflow: auto; max-height: 300px;">
                        <table class="table" >
                            <thead>
                                <tr>
                                    <th>Documento(DUI)</th>
                                    <th>Nombre</th>                                   
                                    <th>Apellido</th>                                   
                                    <th>Telefono</th>                                   
                                    <th>Direccion</th>                                   
                                    <th>Disponibilidad</th>                                   
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%for (Motorista registro : daoMotorista.mostrar_Motorista()) {%>
                                <tr>
                                    <td><%= registro.getDui_motorista()%></td>
                                    <td><%= registro.getNombre_motorista()%></td>
                                    <td><%= registro.getApellido_motorista()%></td>
                                    <td><%= registro.getTelefono()%></td>
                                    <td><%= registro.getDireccion_motorista()%></td>
                                    <!--<td><  registro.getDisponibilidad_motorista()%></td>-->
                                    <%if (registro.getDisponibilidad_motorista()) {%>
                                    <td>ACTIVO</td>
                                    <%} else {%>
                                    <td>INACTIVO</td>
                                    <%}%>

                                    <td><div align="right">
                                            <button class="btn btn-primary readMore roundBtn"
                                                    name="editar" value="<%=registro.getDui_motorista()%>">Editar</button>
                                        </div>
                                    <td><div align="right">
                                            <button class="btn btn-primary readMore roundBtn"                                                    
                                                    name="borrar" value="<%=registro.getDui_motorista()%>">Borrar</button>
                                        </div>
                                </tr>
                                <%}%>
                            </tbody>
                        </table>
                    </div>  
                </form>
            </div>
        </div>
    </body>
</html>
