<%-- 
    Document   : CRUDetalleExcursion
    Created on : 10-ene-2021, 20:59:59
    Author     : EQUIPO
--%>

<%@page import="modelo.entidades.Motorista"%>
<%@page import="modelo.entidades.Transporte"%>
<%@page import="modelo.dao.DAOTransporte"%>
<%@page import="modelo.entidades.Reservaciones"%>
<%@page import="modelo.dao.DAODetalleExcursion"%>
<%@page import="modelo.dao.DAO_Reservaciones"%>
<%@page import="modelo.dao.DAO_Motorista"%>
<%@page import="modelo.entidades.DetalleExcursion"%>
<!DOCTYPE html>
<%
    DAODetalleExcursion daodetalle = new DAODetalleExcursion();
    DetalleExcursion modificar = (DetalleExcursion) request.getAttribute("modificar");//OJO AQUI
    DAO_Reservaciones daoreserva = new DAO_Reservaciones();
    DAO_Motorista daomotorista = new DAO_Motorista();
    DAOTransporte daotrasnsporte = new DAOTransporte();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Detalle Excursion</title>
        <link rel="stylesheet" type="text/css" href="recursos/css/core.css">
        <link rel="stylesheet" type="text/css" href="recursos/css/bootstrap.css">
        <link href="recursos/css/table.css" rel="stylesheet">
    </head>
    <body>
        <script>
            function mayus(e) {
                e.value = e.value.toUpperCase();
            }
        </script>
        <div class="message-bar">
            <span class="post"><%=(modificar == null) ? "REGISTRO" : "MODIFICACION"%> DE CONFIRMACION DE VIAJES.</span>
        </div> 
        <%---------------- FIN EN CABEZADO DE LA PAGINA ----------------------%>
        <jsp:include page="menu_secundario_admin.html"></jsp:include>
            <div class="container">
                <div class="col-lg-12">

                    <div align="center"><h1><%=(modificar == null) ? "NUEVO " : "EDITAR "%>CONFIRMACION</h1> </div>

                <%------------------- INICIO FORMULARIO 1 --------------------%>
                <form action="Servlet_DetalleExcursion" method="post">


                    <input value="<%=modificar != null ? modificar.getIddetalle() : ""%>" type="text" name="id"  style="display:none"><br>

                    <%-- <div class="inputform">
                        <label>ID Confirmacion: </label>
                        <input value="<%=(modificar == null) ? "" : modificar.getIddetalle()%>" type="text" class="input" name="id_detalle" 
                               placeholder="" required>
                        <span class="form_hint"></span>
                    </div>
                    --%>
                    <div class="inputform">
                        <label>Codigo de Reservacion: </label>
                        <select id="id_combo_reserva" name="id_reserva" class="select-css" required="true">
                            <% if (modificar != null) {
                                    for (Reservaciones x : daoreserva.mostrar_Reservacion()) {
                                        if (x.getId_reservaciones() == modificar.getIdreservacion().getId_reservaciones()) {%>
                            <option selected value="<%=x.getId_reservaciones()%>"><%=x.getId_reservaciones()%></option>
                            <%} else {%>
                            <option value="<%=x.getId_reservaciones()%>"><%=x.getId_reservaciones()%></option>
                            <%}
                                }
                            } else {
                                for (Reservaciones y : daoreserva.mostrarReservacionSinConfirmar()) {%>
                            <option value="<%=y.getId_reservaciones()%>"> <%=y.getId_reservaciones()%> </option>
                            <%}
                                    }%>
                        </select>
                    </div>   

                    <div class="inputform">
                        <label>Placa Transporte: </label>
                        <select id="id_combo_transporte" name="id_transporte" class="select-css" required="true">
                            <% for (Transporte x : daotrasnsporte.mostrarTransporte()) {
                                    if (modificar != null) {
                                        if (x.getPlaca() == modificar.getPlaca().getPlaca()) {%>
                            <option selected value="<%=x.getPlaca()%>"><%=x.getPlaca()%></option>
                            <%} else {%>
                            <option value="<%=x.getPlaca()%>"><%=x.getPlaca()%></option>
                            <%}%>
                            <%} else {%>
                            <option value="<%=x.getPlaca()%>"><%=x.getPlaca()%></option>
                            <%}
                                }%>
                        </select>
                    </div> 

                    <div class="inputform">
                        <label>DUI Motorista: </label>
                        <select id="id_combo_motorista" name="id_motorista" class="select-css" required="true">
                            <% for (Motorista x : daomotorista.mostrar_Motorista()) {
                                    if (modificar != null) {
                                        if (x.getDui_motorista() == modificar.getDuimotorista().getDui_motorista()) {%>
                            <option selected value="<%=x.getDui_motorista()%>"><%=x.getDui_motorista()%></option>
                            <%} else {%>getDui_motorista()
                            <option value="<%=x.getDui_motorista()%>"><%=x.getDui_motorista()%></option>
                            <%}%>
                            <%} else {%>
                            <option value="<%=x.getDui_motorista()%>"><%=x.getDui_motorista()%></option>
                            <%}
                                }%>
                        </select>
                    </div> 

                    <div class="inputform">
                        <label>Confirmacion?: </label>
                        <select id="id_combo_confirma" name="confirma" class="select-css" required="true">
                            <% if (modificar != null) {
                                    if (modificar.isConfirmacion()) {%>
                            <option selected value="TRUE">Afirmar</option>
                            <option value="FALSE">Denegar</option>
                            <%} else {%>
                            <option selected value="FALSE">Denegar</option>
                            <option value="TRUE">Afirma</option>
                            <%}
                            } else {%>
                            <option value="TRUE">Afirma</option>
                            <option value="FALSE">Denegar</option>
                            <%}%>
                        </select>
                    </div> 

                    <%------------------- INICIO TABLA 1 ---------------------%>
                    <table>
                        <tr>
                            <td width="48%"></td>
                            <td>
                                <input onclick="validacion()" value="<%=(modificar == null) ? "Registrar" : "Modificar"%>"
                                       name="<%=(modificar == null) ? "registrar" : "modificar"%>"
                                       type="submit" class="submit btn btn-primary readMore roundBtn">
                            </td>
                            <td>
                                <input value="Cancelar" 
                                       name="cancelar"
                                       type="submit" class="submit btn btn-primary readMore roundBtn">
                            </td>
                            <td width="48%"></td>
                        </tr>
                    </table>
                    <%------------------- FIN TABLA 1 ------------------------%>
                </form><br>
                <%------------------- FIN FORMULARIO 1 E INICIO 2 -----------------------%>
                <form action="Servlet_DetalleExcursion" method="post">
                    <div style="<%=(daodetalle.mostrarDetalle().isEmpty()) ? "display:none" : "display:block"%>;
                         overflow: auto; max-height: 300px;">
                        <table class="table" >
                            <thead>
                                <tr>
                                    <th>ID</th>                                   
                                    <th>Estado</th>
                                    <th>ID Reserva</th>
                                    <th>Nombre Lugar</th>
                                    <th>Nombre Cliente</th>
                                    <th>Placa Vehiculo</th>
                                    <th>Nombre Motorista</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%for (DetalleExcursion registro : daodetalle.mostrarDetalle()) {%>
                                <tr>

                                    <td><%= registro.getIddetalle()%></td>
                                    <%if (registro.isConfirmacion()) {%>
                                    <td>Aceptado</td>
                                    <%} else {%>
                                    <td>Denegado</td>
                                    <%}%>
                                    <td><%= registro.getIdreservacion().getId_reservaciones()%></td>
                                    <td><%= registro.getIdreservacion().getLugares().getNombrelugar()%></td>
                                    <td><%= registro.getIdreservacion().getCliente().getNombre_cliente() + registro.getIdreservacion().getCliente().getApellido_cliente()%></td>
                                    <td><%= registro.getPlaca().getPlaca()%></td>
                                    <td><%= registro.getDuimotorista().getNombre_motorista() + registro.getDuimotorista().getApellido_motorista()%></td>

                                    <td><div align="right">
                                            <button class="btn btn-primary readMore roundBtn"
                                                    name="editar" value="<%=registro.getIddetalle()%>">Editar</button>
                                        </div>
                                    <td><div align="right">
                                            <button class="btn btn-primary readMore roundBtn"                                                    
                                                    name="borrar" value="<%=registro.getIddetalle()%>">Borrar</button>
                                        </div>
                                </tr>
                                <%}%>
                            </tbody>
                        </table>
                    </div>  
                </form>
            </div>
        </div>
    </body>
</html>
