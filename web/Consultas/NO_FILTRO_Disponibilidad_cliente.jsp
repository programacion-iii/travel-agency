<%-- 
    Document   : NO_FILTRO_Disponibilidad_cliente_motorista
    Created on : 01-17-2021, 10:36:51 PM
    Author     : BLADIMIR
--%>

<%@page import="modelo.dao.DAODetalleExcursion"%>
<%@page import="modelo.entidades.DetalleExcursion"%>
<%@page import="modelo.entidades.Reservaciones"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    DAODetalleExcursion daodetalle = new DAODetalleExcursion();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Disponiblidad de clientes</title>
        <link href="../recursos/css/table.css" rel="stylesheet">
    </head>
    <body>
    <center><h1>DISPONIBILIAD DE CLIENTES</h1></center><hr><br>
        <%------------------------INICIO TABLA----------------------------%>
    <table>
        <thead>
            <tr class="table table-bordered table-responsive">
                <th>ID</th>                                   
                <th>ESTADO</th>
                <th>ID RESERVACION</th>
                <th>LUGAR</th>
                <th>CLIENTE</th>
                <th>PLACA DEL VEHICULO</th>
                <th>MOTORISTA</th>
            </tr>
        </thead>
        <tbody>
            <%for (DetalleExcursion registro : daodetalle.mostrarDetalle()) {%>
            <tr class="table table-bordered table-responsive">
                <td><%= registro.getIddetalle()%></td>
                <%if (registro.isConfirmacion()) {%>
                <td>Aceptado</td>
                <%} else {%>
                <td>Denegado</td>
                <%}%>
                <td><%= registro.getIdreservacion().getId_reservaciones()%></td>
                <td><%= registro.getIdreservacion().getLugares().getNombrelugar()%></td>
                <td><%= registro.getIdreservacion().getCliente().getNombre_cliente() + registro.getIdreservacion().getCliente().getApellido_cliente()%></td>
                <td><%= registro.getPlaca().getPlaca()%></td>
                <td><%= registro.getDuimotorista().getNombre_motorista() + registro.getDuimotorista().getApellido_motorista()%></td>
            </tr>
            <%}%>
        </tbody>
    </table>
    <%-----------------------------FIN TABLA-----------------------------%>
</body>
</html>
