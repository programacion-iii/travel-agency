<%-- 
    Document   : Reservaciones_de_clientes
    Created on : 01-17-2021, 08:30:46 PM
    Author     : BLADIMIR
--%>

<%@page import="modelo.entidades.Reservaciones"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reservaciones de Clientes</title>
        <link href="recursos/css/table.css" rel="stylesheet">
    </head>
    <body>
    <center><h1>RESERVACIONES ACTUALES DE LOS CLIENTES</h1></center><hr><br>
        <%------------------------INICIO TABLA----------------------------%>
    <table>
        <thead>
            <tr class="table table-bordered table-responsive">
                <th>ID RESERVACION</th>
                <th>CLIENTE</th>   
                <th>LUGAR</th>                                   
                <th>HORA</th>
                <th>FECHA</th>
                <th>MONTO</th>
                <th>DIRECCION DE PARTIDA</th> 
            </tr>
        </thead>
        <tbody>
            <%
                List<Reservaciones> lista_reservaciones = (List<Reservaciones>) request.getAttribute("lista_all_reservaciones");
                for (Reservaciones registro : lista_reservaciones) {
            %>
            <tr class="table table-bordered table-responsive">

                <td><%= registro.getId_reservaciones()%></td>
                <td><%= registro.getCliente().getNombre_cliente()%></td>
                <td><%= registro.getLugares().getNombrelugar()%></td>
                <td><%= registro.getHora()%></td>
                <td><%= registro.getFecha()%></td>
                <td><%= registro.getMonto()%></td>
                <td><%= registro.getDireccion_encuentro()%></td>
            </tr>
            <%}%>
        </tbody>
    </table>
    <%-----------------------------FIN TABLA-----------------------------%>
</body>
</html>
