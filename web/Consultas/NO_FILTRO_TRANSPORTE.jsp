<%-- 
    Document   : NO_FILTRO_TRANSPORTE
    Created on : 01-17-2021, 11:27:42 PM
    Author     : BLADIMIR
--%>

<%@page import="modelo.dao.DAOTransporte"%>
<%@page import="modelo.entidades.Transporte"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    DAOTransporte daotransporte = new DAOTransporte();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>TRANSPORTE DISPONIBLE</title>
        <link href="../recursos/css/table.css" rel="stylesheet">
    </head>
    <body>
    <center><h1>DISPONIBILIAD DE TRANSPORTE</h1></center><hr><br>
        <%------------------------INICIO TABLA----------------------------%>
    <table class="table" >
        <thead>
            <tr>
                <th>Placa</th>
                <th>Tipo</th>                                   
                <th>Estado</th>                                   
                <th>Capacidad</th>                                  
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <%for (Transporte registro : daotransporte.mostrarTransporte()) {%>
            <tr>
                <td><%= registro.getPlaca()%></td>
                <td><%= registro.getId_tipo().getNombre_tipo()%></td>
                <%if (registro.isDisponibilidad()) {%>
                <td>ACTIVO</td>
                <%} else {%>
                <td>INACTIVO</td>
                <%}%>
                <td><%= registro.getId_tipo().getCapacidad()%></td>
               
            </tr>
            <%}%>
        </tbody>
    </table>
    <%-----------------------------FIN TABLA-----------------------------%>
</body>
</html>
