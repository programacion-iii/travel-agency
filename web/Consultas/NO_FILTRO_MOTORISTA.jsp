<%-- 
    Document   : NO_FILTRO_MOTORISTA
    Created on : 01-17-2021, 11:27:42 PM
    Author     : BLADIMIR
--%>

<%@page import="modelo.dao.DAO_Motorista"%>
<%@page import="modelo.entidades.Motorista"%>
<%@page import="modelo.dao.DAOTransporte"%>
<%@page import="modelo.entidades.Transporte"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    DAO_Motorista daoMotorista = new DAO_Motorista();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MOTORISTAS DISPONIBLE</title>
        <link href="../recursos/css/table.css" rel="stylesheet">
    </head>
    <body>
    <center><h1>DISPONIBILIAD DE MOTORISTA</h1></center><hr><br>
        <%------------------------INICIO TABLA----------------------------%>
    <table class="table" >
        <thead>
            <tr>
                <th>Documento(DUI)</th>
                <th>Nombre</th>                                   
                <th>Apellido</th>                                   
                <th>Telefono</th>                                   
                <th>Direccion</th>                                   
                <th>Disponibilidad</th>                                   
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <%for (Motorista registro : daoMotorista.mostrar_Motorista()) {%>
            <tr>
                <td><%= registro.getDui_motorista()%></td>
                <td><%= registro.getNombre_motorista()%></td>
                <td><%= registro.getApellido_motorista()%></td>
                <td><%= registro.getTelefono()%></td>
                <td><%= registro.getDireccion_motorista()%></td>
                <!--<td><  registro.getDisponibilidad_motorista()%></td>-->
                <%if (registro.getDisponibilidad_motorista()) {%>
                <td>ACTIVO</td>
                <%} else {%>
                <td>INACTIVO</td>
                <%}%>
            </tr>
            <%}%>
        </tbody>
    </table>
    <%-----------------------------FIN TABLA-----------------------------%>
</body>
</html>
