<%-- 
    Document   : CRUDLugares
    Created on : 11-21-2020, 05:44:04 PM
    Author     : CRUZ
--%>

<%@page import="modelo.entidades.Clasificacion"%>
<%@page import="modelo.dao.DAOClasificacion"%>
<%@page import="modelo.entidades.Lugares"%>
<%@page import="modelo.dao.DAOLugares"%>
<!DOCTYPE html>
<%
    DAOLugares daolugares = new DAOLugares();
    Lugares modificar = (Lugares) request.getAttribute("modificar");
    DAOClasificacion daoClasificacion = new DAOClasificacion();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>CRUD LUGARES</title>
        <link rel="stylesheet" type="text/css" href="recursos/css/core.css">
        <link rel="stylesheet" type="text/css" href="recursos/css/bootstrap.css">
        <link href="recursos/css/table.css" rel="stylesheet">
        <link href="recursos/combo/select_css.css" rel="stylesheet">
    </head>
    <body>
        <script>
            function mayus(e) {
                e.value = e.value.toUpperCase();
            }
        </script>
        <div class="message-bar">
            <span class="post"><%=(modificar == null) ? "REGISTRO" : "MODIFICACION"%> DE LUGARES</span>
        </div> 
        <%---------------- FIN EN CABEZADO DE LA PAGINA ----------------------%>
        <jsp:include page="menu_secundario_admin.html"></jsp:include>
            <div class="container">
                <div class="col-lg-12">

                    <div align="center"><h1><%=(modificar == null) ? "Nuevo " : "Editar "%>Lugar</h1> </div>

                <%------------------- INICIO FORMULARIO 1 --------------------%>
                <form action="Servlet_Lugares" method="post">


                    <input value="<%=modificar != null ? modificar.getIdLugares() : ""%>" type="text" name="idLugar"  style="display:none"><br>


                    <div class="inputform">
                        <label>ID LUGARES: </label>
                        <input value="<%=(modificar == null) ? "" : modificar.getIdLugares()%>" type="text" class="input" name="id_lugares" 
                               placeholder="" required="true" pattern="[0-9]{1,200000}"  title="[0-9]">
                        <span class="form_hint"></span>
                    </div>


                    <div  class="inputform">
                        <label>Nombre: </label>
                        <input value="<%=(modificar == null) ? "" : modificar.getNombrelugar()%>" type="text" class="input" name="nombre_lugar" 
                               placeholder="" required="true" pattern="[a-�-z A-�-Z]{2,254}" onkeyup="mayus(this);">
                        <span class="form_hint"></span>
                    </div>

                    <div  class="inputform">
                        <label>Descripcion: </label>
                        <input value="<%=(modificar == null) ? "" : modificar.getDescripcion()%>" type="text" class="input" name="descripcion_lugar" 
                               placeholder="" required="true" pattern="[a-�-z A-�-Z]{2,254}" onkeyup="mayus(this);">
                        <span class="form_hint"></span>
                    </div>

                    <div class="inputform">
                        <label>Clasificacion:</label>
                        <select id="id_combo" name="combClasi" class="select-css" required="true">
                            <% for (Clasificacion clasificacion : daoClasificacion.mostrar_Clasificacion()) {%>
                            <option value="<%=clasificacion.getIdClasificacion()%>"><%=clasificacion.getNombreClas()%></option>
                            <%}%>
                        </select>
                    </div> 

                    <div  class="inputform">
                        <label>Direccion: </label>
                        <input value="<%=(modificar == null) ? "" : modificar.getDireccion()%>" type="text" class="input" name="direccion_lugar" 
                               placeholder="" required="true" onkeyup="mayus(this);">
                        <span class="form_hint"></span>
                    </div>

                    <%------------------- INICIO TABLA 1 ---------------------%>
                    <table>
                        <tr>
                            <td width="48%"></td>
                            <td>
                                <input value="<%=(modificar == null) ? "Registrar" : "Modificar"%>"
                                       name="<%=(modificar == null) ? "registrar" : "modificar"%>"
                                       type="submit" class="submit btn btn-primary readMore roundBtn">
                            </td>
                            <td>
                                <input value="Cancelar" 
                                       name="cancelar"
                                       type="submit" class="submit btn btn-primary readMore roundBtn">
                            </td>
                            <td width="48%"></td>
                        </tr>
                    </table>
                    <%------------------- FIN TABLA 1 ------------------------%>
                </form><br>
                <%------------------- FIN FORMULARIO 1 -----------------------%>
                <form action="Servlet_Lugares" method="post">
                    <div style="<%=(daolugares.mostrar_Lugares().isEmpty()) ? "display:none" : "display:block"%>;
                         overflow: auto; max-height: 300px;">
                        <table class="table" >
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>                                   
                                    <th>Descripcion</th>                                   
                                    <th>Clasificacion</th>                                   
                                    <th>Direccion</th>                                   

                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%for (Lugares registro : daolugares.mostrar_Lugares()) {%>
                                <tr>
                                    <td><%= registro.getIdLugares()%></td>
                                    <td><%= registro.getNombrelugar()%></td>
                                    <td><%= registro.getDescripcion()%></td>
                                    <td><%= registro.getIdclasificacion().getNombreClas()%></td>
                                    <td><%= registro.getDireccion()%></td>

                                    <td><div align="right">
                                            <button class="btn btn-primary readMore roundBtn"
                                                    name="editar" value="<%=registro.getIdLugares()%>">Editar</button>
                                        </div>
                                    <td><div align="right">
                                            <button class="btn btn-primary readMore roundBtn"                                                    
                                                    name="borrar" value="<%=registro.getIdLugares()%>">Borrar</button>
                                        </div>
                                </tr>
                                <%}%>
                            </tbody>
                        </table>
                    </div>  
                </form>
            </div>
        </div>
    </body>
</html>
